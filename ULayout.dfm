object FormPrincipal: TFormPrincipal
  Left = 0
  Top = 0
  Caption = 'Layout'
  ClientHeight = 277
  ClientWidth = 565
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 34
    Width = 226
    Height = 16
    Caption = 'Referencia Empresa (Longitud 10):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 115
    Top = 64
    Width = 119
    Height = 16
    Caption = 'Clave Beneficiario:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 131
    Top = 98
    Width = 101
    Height = 16
    Caption = 'Tipo de Cuenta:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 53
    Top = 129
    Width = 181
    Height = 16
    Caption = 'Numero de Banco Receptor:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 426
    Top = 11
    Width = 85
    Height = 16
    Caption = 'Tipo Registro'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 110
    Top = 2
    Width = 92
    Height = 16
    Caption = 'Identificador: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object labelid: TLabel
    Left = 206
    Top = 2
    Width = 8
    Height = 16
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 103
    Top = 159
    Width = 131
    Height = 16
    Caption = 'Fecha de Aplicacion:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 264
    Width = 565
    Height = 13
    Panels = <
      item
        Width = 50
      end>
  end
  object btnGenerar: TButton
    Left = 206
    Top = 193
    Width = 81
    Height = 25
    Caption = 'Guardar'
    TabOrder = 1
    OnClick = btnGenerarClick
  end
  object txtReferencia: TEdit
    Left = 240
    Top = 33
    Width = 139
    Height = 21
    Enabled = False
    MaxLength = 10
    TabOrder = 2
  end
  object txtClaveBeneficiario: TEdit
    Left = 240
    Top = 62
    Width = 139
    Height = 21
    Enabled = False
    MaxLength = 20
    TabOrder = 3
  end
  object cbTipoCuenta: TComboBox
    Left = 240
    Top = 96
    Width = 251
    Height = 21
    Enabled = False
    TabOrder = 4
    Items.Strings = (
      'CUENTA DE CHEQUES (11 POSICIONES)'
      'CUENTA (TARJETA) DE DEBITO (16 POSICIONES)'
      'CUENTA DE CHEQUES CLABE (18 POSICIONES)')
  end
  object cbBancoReceptor: TComboBox
    Left = 240
    Top = 128
    Width = 313
    Height = 21
    Enabled = False
    TabOrder = 5
    Items.Strings = (
      'Banco Nacional de Mexico, S.A.'
      'Banco Nacional deComercio Exterior., SNC'
      'Bancomer, S.A.'
      'Banco Santander, S.A.'
      'Banco Nacional del Ejercito, Fuerza Aerea y Armada, S.N.C.'
      'HSBC (Mexico), S.A.'
      'Banco del Baj'#237'o, S.A.'
      'IXE Banco, S.A.'
      'Banco Inbursa, S.A.'
      'Bnaco Interacciones, S.A.'
      'Banca Mifel, S.A.'
      'Scotiabank Iverlat, S.A.'
      'Banco Regional de Monterrey, S.A.'
      'Banco Invex, S.A.'
      'Bansi, S.A.'
      'Banca Afirme, S.A.'
      'Banco Mercantil del Norte, S.A.'
      'The Royal Bank of Scotland Mexico S.A. Inst. de Banca Multiple.'
      'American Express Bank (Mexico), S.A.'
      'Bank of America (Mexico), S.A.'
      'J.P. Morgan, S.A.'
      'Banco Ve por Mas., S.A.'
      'Banco Azteca, S.A.'
      'Banco Ahorro Famsa, S.A.'
      'Banco Multiva, S.A. Institucion de Banca Multiple.'
      'Banco Wal-Mart de Mexico Adelante, S.A.'
      'Nacional Financiera, S.N.C.'
      'BanCoppel, S.A.'
      'Banco del Ahorro Nacional y Servicios Financieros.')
  end
  object cbTipoRegistro: TComboBox
    Left = 426
    Top = 33
    Width = 93
    Height = 21
    ItemIndex = 0
    TabOrder = 6
    Text = 'Detalle Alta'
    Items.Strings = (
      'Detalle Alta'
      'Detalle Baja')
  end
  object dtpFechaAplicacion: TDateTimePicker
    Left = 240
    Top = 158
    Width = 139
    Height = 21
    Date = 43777.397405578700000000
    Time = 43777.397405578700000000
    TabOrder = 7
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=E:\Microsip datos 2018\AD2007.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 8
    Top = 190
  end
  object qryNomina: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_dispersion where pago_nomina_id=:id')
    Left = 64
    Top = 192
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryNominaPAGO_NOMINA_ID: TIntegerField
      FieldName = 'PAGO_NOMINA_ID'
      Origin = 'PAGO_NOMINA_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryNominaNUMERO_CONTRATO: TStringField
      FieldName = 'NUMERO_CONTRATO'
      Origin = 'NUMERO_CONTRATO'
      Size = 5
    end
    object qryNominaSECUENCIA: TStringField
      FieldName = 'SECUENCIA'
      Origin = 'SECUENCIA'
      Size = 2
    end
    object qryNominaREFERENCIA_EMPRESA: TStringField
      FieldName = 'REFERENCIA_EMPRESA'
      Origin = 'REFERENCIA_EMPRESA'
      Size = 10
    end
    object qryNominaCLAVE_BENEFICIARIO: TStringField
      FieldName = 'CLAVE_BENEFICIARIO'
      Origin = 'CLAVE_BENEFICIARIO'
    end
    object qryNominaTIPO_CUENTA: TStringField
      FieldName = 'TIPO_CUENTA'
      Origin = 'TIPO_CUENTA'
      Size = 1
    end
    object qryNominaNUMERO_BANCO_RECEPTOR: TStringField
      FieldName = 'NUMERO_BANCO_RECEPTOR'
      Origin = 'NUMERO_BANCO_RECEPTOR'
      Size = 3
    end
  end
  object qryEmpleado: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select numero,apellido_paterno,apellido_materno,nombres from emp' +
        'leados e'
      
        'join pagos_nomina pn on pn.empleado_id=e.empleado_id where pn.pa' +
        'go_nomina_id=:pid')
    Left = 120
    Top = 216
    ParamData = <
      item
        Name = 'PID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryEmpleadoNUMERO: TIntegerField
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
      Required = True
    end
    object qryEmpleadoAPELLIDO_PATERNO: TStringField
      FieldName = 'APELLIDO_PATERNO'
      Origin = 'APELLIDO_PATERNO'
      Required = True
      Size = 30
    end
    object qryEmpleadoAPELLIDO_MATERNO: TStringField
      FieldName = 'APELLIDO_MATERNO'
      Origin = 'APELLIDO_MATERNO'
      Size = 30
    end
    object qryEmpleadoNOMBRES: TStringField
      FieldName = 'NOMBRES'
      Origin = 'NOMBRES'
      Required = True
      Size = 30
    end
  end
end
