unit Uselempresa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.FMTBcd, Data.DB, Data.SqlExpr,
  Vcl.StdCtrls, Vcl.DBCtrls, Data.DBXFirebird, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Comp.Client, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait, UPrincipal;

type
  TSeleccionaEmpresa = class(TForm)
    Label1: TLabel;
    ListEmpresas: TDBLookupListBox;
    btnAceptar: TButton;
    btnCancelar: TButton;
    DataSource: TDataSource;
    FDConnection: TFDConnection;
    FDQuery: TFDQuery;
    FDTransaction: TFDTransaction;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure ListEmpresasKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SeleccionaEmpresa: TPrincipal;

implementation

{$R *.dfm}

procedure TSeleccionaEmpresa.btnAceptarClick(Sender: TObject);
var
  FormPrincipal : TPrincipal;
begin
  FormPrincipal := TPrincipal.Create(nil);
  FormPrincipal.Conexion.Params.Values['Database'] := FDConnection.Params.Values['Database'].Replace('System\config.fdb','') + ListEmpresas.SelectedItem + '.fdb';
  FormPrincipal.Conexion.Params.Values['Server'] := FDConnection.Params.Values['Server'];
  FormPrincipal.Conexion.Params.Values['User_Name'] := FDConnection.Params.Values['User_Name'];
  FormPrincipal.Conexion.Params.Values['Password'] := FDConnection.Params.Values['Password'];
  FormPrincipal.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  //PRUEBA CONEXION ALTERNA
  {FormPrincipal.ConexionDetallado.Params.Values['Database'] := FDConnection.Params.Values['Database'].Replace('System\config.fdb','') + ListEmpresas.SelectedItem + '.fdb';
  FormPrincipal.ConexionDetallado.Params.Values['Server'] := FDConnection.Params.Values['Server'];
  FormPrincipal.ConexionDetallado.Params.Values['User_Name'] := FDConnection.Params.Values['User_Name'];
  FormPrincipal.ConexionDetallado.Params.Values['Password'] := FDConnection.Params.Values['Password'];
  FormPrincipal.ConexionDetallado.Params.Values['RoleName'] := 'USUARIO_MICROSIP';}

  FormPrincipal.StatusBar1.Panels.Items[0].Text := ListEmpresas.SelectedItem;
  FormPrincipal.Visible := True;
  FormPrincipal.Show;
  Hide;
end;

procedure TSeleccionaEmpresa.btnCancelarClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TSeleccionaEmpresa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
Application.Terminate;
end;

procedure TSeleccionaEmpresa.FormShow(Sender: TObject);
begin
  FDQuery.Open;
end;
procedure TSeleccionaEmpresa.ListEmpresasKeyPress(Sender: TObject;
  var Key: Char);
begin
   if Key = #13 then btnAceptar.SetFocus;
end;

end.
