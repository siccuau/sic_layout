unit UEmpleado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.StdCtrls;

type
  TFormEmpleado = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    btnGuardar: TButton;
    Label6: TLabel;
    Label4: TLabel;
    labelid: TLabel;
    txtNumeroEmpleado: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    rfc:string;
  end;

var
  FormEmpleado: TFormEmpleado;

implementation

{$R *.dfm}

procedure TFormEmpleado.btnGuardarClick(Sender: TObject);
begin
if txtNumeroEmpleado.Text<>'' then
  begin
  if Conexion.ExecSQLScalar('select count(rfc) from sic_dispersion where rfc=:rfc',[rfc])=0 then
    begin
    Conexion.ExecSQL('insert into sic_dispersion(rfc,num_empleado) values(:rfc,:numempleado)',[rfc,txtNumeroEmpleado.Text]);
          ShowMessage('Guardado correctamente.');
                self.close;
    end
   else
      begin
      Conexion.ExecSQL('update sic_dispersion set num_empleado=:num where rfc=:rfc',[txtNumeroEmpleado.Text,rfc]);
      ShowMessage('Guardado correctamente.');
      self.close;
      end;
  end
  else
  ShowMessage('El numero de empleado no puede estar vacio.');
  end;

procedure TFormEmpleado.FormShow(Sender: TObject);
begin
labelid.Caption:=rfc;
if Conexion.ExecSQLScalar('select count(num_empleado) from sic_dispersion where rfc=:rfc',[rfc])>0 then
  begin
  txtNumeroEmpleado.Text:=Conexion.ExecSQLScalar('select num_empleado from sic_dispersion where rfc=:rfc',[rfc]);
  end;
end;

end.
