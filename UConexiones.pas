unit UConexiones;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, registry, System.UITypes;

type
  TConexiones = class(TForm)
    txtNombre: TEdit;
    txtServidor: TEdit;
    txtCarpetaMSP: TEdit;
    cb_tipo_conexion: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    btnAceptar: TButton;
    Button2: TButton;
    btnEliminar: TButton;
    procedure btnAceptarClick(Sender: TObject);
    procedure cb_tipo_conexionChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Conexiones: TConexiones;

implementation

{$R *.dfm}



procedure TConexiones.btnAceptarClick(Sender: TObject);
var
  reg:TRegistry;
begin
  reg:=TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
  reg.RootKey := HKEY_LOCAL_MACHINE;
  if (not reg.OpenKey('SOFTWARE\SIC\Conexiones\'+txtNombre.Text,False)) then
  begin
    reg.CreateKey('SOFTWARE\SIC\Conexiones\'+txtNombre.Text);
  end
  else
  begin
     reg.OpenKeyReadOnly('SOFTWARE\SIC\Conexiones\'+txtNombre.Text)
  end;
  reg.OpenKey('SOFTWARE\SIC\Conexiones\'+txtNombre.Text, True);
  reg.WriteString('Datos',txtCarpetaMSP.Text);
  reg.WriteString('Servidor',txtServidor.Text);
  reg.WriteString('Tipo',cb_tipo_conexion.Text);
  ModalResult := mrOK;
end;

procedure TConexiones.btnEliminarClick(Sender: TObject);
var
  reg:TRegistry;
begin
   reg:=TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
   reg.RootKey := HKEY_LOCAL_MACHINE;
   if messagedlg('Desea eliminar esta Conexion?',mtError, mbOKCancel, 0)= mrOK then
   begin
      reg.DeleteKey('SOFTWARE\SIC\Conexiones\'+txtNombre.Text);
      ShowMessage('Conexion Eliminada');
      ModalResult := mrOK;
   end;
end;

procedure TConexiones.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TConexiones.cb_tipo_conexionChange(Sender: TObject);
begin
  if (cb_tipo_conexion.Text = 'Local') then
  begin
    Label3.Enabled:=False;
    txtServidor.Enabled:=False;
  end
  else
  begin
    Label3.Enabled:=True;
    txtServidor.Enabled:=True;
  end;
end;

end.
