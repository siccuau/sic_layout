unit ULayout;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.ComCtrls, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TFormPrincipal = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    btnGenerar: TButton;
    Label2: TLabel;
    txtReferencia: TEdit;
    txtClaveBeneficiario: TEdit;
    Label3: TLabel;
    Label5: TLabel;
    cbTipoCuenta: TComboBox;
    Label6: TLabel;
    cbBancoReceptor: TComboBox;
    qryNomina: TFDQuery;
    qryNominaPAGO_NOMINA_ID: TIntegerField;
    qryNominaNUMERO_CONTRATO: TStringField;
    qryNominaSECUENCIA: TStringField;
    qryNominaREFERENCIA_EMPRESA: TStringField;
    qryNominaCLAVE_BENEFICIARIO: TStringField;
    qryNominaTIPO_CUENTA: TStringField;
    qryNominaNUMERO_BANCO_RECEPTOR: TStringField;
    cbTipoRegistro: TComboBox;
    Label7: TLabel;
    Label4: TLabel;
    labelid: TLabel;
    Label8: TLabel;
    dtpFechaAplicacion: TDateTimePicker;
    qryEmpleado: TFDQuery;
    qryEmpleadoNUMERO: TIntegerField;
    qryEmpleadoAPELLIDO_PATERNO: TStringField;
    qryEmpleadoAPELLIDO_MATERNO: TStringField;
    qryEmpleadoNOMBRES: TStringField;
    procedure btnGenerarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    function obtenerSiglas (cadena : string): string;
  private

  public

    nomina_id:integer;
    importe,fecha,rfc,nombre_beneficiario,cuenta_beneficiario,concepto_pago,email:string;
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

{$R *.dfm}

procedure TFormPrincipal.btnGenerarClick(Sender: TObject);
var
tipocuenta,num_banco_receptor,tipo_registro,fecha1:string;
begin
    case cbTipoCuenta.ItemIndex of
    0:tipocuenta:='1';
    1:tipocuenta:='3';
    2:tipocuenta:='9';
    end;

    case cbTipoRegistro.ItemIndex of
    0:tipo_registro:='DA';
    1:tipo_registro:='DB';
    end;

        case cbBancoReceptor.ItemIndex of
    0:num_banco_receptor:='002';
    1:num_banco_receptor:='006';
    2:num_banco_receptor:='012';
    3:num_banco_receptor:='014';
    4:num_banco_receptor:='019';
    5:num_banco_receptor:='021';
    6:num_banco_receptor:='030';
    7:num_banco_receptor:='032';
    8:num_banco_receptor:='036';
    9:num_banco_receptor:='037';
    10:num_banco_receptor:='042';
    11:num_banco_receptor:='044';
    12:num_banco_receptor:='058';
    13:num_banco_receptor:='059';
    14:num_banco_receptor:='060';
    15:num_banco_receptor:='062';
    16:num_banco_receptor:='072';
    17:num_banco_receptor:='102';
    18:num_banco_receptor:='103';
    19:num_banco_receptor:='106';
    20:num_banco_receptor:='110';
    21:num_banco_receptor:='113';
    22:num_banco_receptor:='127';
    23:num_banco_receptor:='131';
    24:num_banco_receptor:='132';
    25:num_banco_receptor:='134';
    26:num_banco_receptor:='135';
    27:num_banco_receptor:='137';
    28:num_banco_receptor:='166';
    end;




    if (Length(txtReferencia.Text)<>10) then
     begin
     ShowMessage('El numero de referencia empresa debe de ser de 10 caracteres.');
     end
      else
      if (cbTipoCuenta.Text='') then
        begin
        ShowMessage('El tipo de cuenta no puede estar vacio.');
        end
          else
          if (cbBancoReceptor.Text='') then
          begin
          ShowMessage('El banco del receptor no puede estar vacio.');
          end
        else
        if (Length(txtClaveBeneficiario.Text)<>20) then
        begin
        ShowMessage('La clave de beneficiario debe de ser de 20 caracteres.');
        end
        else
              begin
               if Conexion.ExecSQLScalar('select count(pago_nomina_id) from sic_dispersion where pago_nomina_id=:id',[nomina_id])>0 then
               begin
               fecha1:=FormatDateTime('YYYY-MM-DD',dtpFechaAplicacion.Date);
                   Conexion.ExecSQL('update sic_dispersion set referencia_empresa=:ref,'+
                                'clave_beneficiario=:cve,tipo_cuenta=:tipo,numero_banco_receptor=:num,tipo_registro=:tip,fecha_aplicacion=:fecha where pago_nomina_id'+
                   '=:pago',[txtReferencia.Text,txtClaveBeneficiario.Text,tipocuenta,num_banco_receptor,tipo_registro,fecha1,nomina_id]);
               end
               else
               begin
                              fecha1:=FormatDateTime('YYYY-MM-DD',dtpFechaAplicacion.Date);
               Conexion.ExecSQL('insert into sic_dispersion(pago_nomina_id,secuencia,referencia_empresa,clave_beneficiario,tipo_cuenta,numero_banco_receptor,tipo_registro,num_empleado,fecha_aplicacion) values(:pago,:sec,:ref,:cve,:tipo,:num,:tip,:emp,:fecha)',[nomina_id,'01',txtReferencia.Text,txtClaveBeneficiario.Text,tipocuenta,num_banco_receptor,tipo_registro,NULL,fecha1]);
               end;
                      self.close;
               end;
          end;



procedure TFormPrincipal.FormShow(Sender: TObject);
var
clave_beneficiario,nombre,fechareferencia:string;
begin
labelid.Caption:=inttostr(nomina_id);
qryNomina.SQL.Text:='select * from sic_dispersion where pago_nomina_id=:id';
qryNomina.ParamByName('id').Value:=nomina_id;
qryNomina.Open;
qryEmpleado.SQL.Text:='select numero,apellido_paterno,apellido_materno,nombres from empleados e'+
' join pagos_nomina pn on pn.empleado_id=e.empleado_id where pn.pago_nomina_id=:pid';
qryEmpleado.ParamByName('pid').Value:=nomina_id;
qryEmpleado.Open;
fechareferencia:=formatdatetime('ddmmyyyy',Now);
txtReferencia.Text:='00'+fechareferencia;
cbTipoCuenta.ItemIndex:=0;
cbBancoReceptor.ItemIndex:=11;
         nombre:=qryEmpleadoNOMBRES.Value+' '+qryEmpleadoAPELLIDO_PATERNO.Value+' '+qryEmpleadoAPELLIDO_MATERNO.Value;
         clave_beneficiario:=uppercase(obtenerSiglas(nombre))+inttostr(qryEmpleadoNUMERO.Value);
         txtClaveBeneficiario.Text:=clave_beneficiario+StringOfChar(' ',20-Length(clave_beneficiario));
  if Conexion.ExecSQLScalar('select count(pago_nomina_id) from sic_dispersion where pago_nomina_id=:id',[nomina_id])>0 then
     begin
      if Conexion.ExecSQLScalar('select count(referencia_empresa) from sic_dispersion where pago_nomina_id=:id',[nomina_id])>0 then
      begin
       //  txtReferencia.Text:=qryNominaREFERENCIA_EMPRESA.Value;
          if Conexion.ExecSQLScalar('select tipo_registro from sic_dispersion where pago_nomina_id=:id',[nomina_id])='DA' then
            begin
            cbTipoRegistro.ItemIndex:=0;
            end
            else
            cbTipoRegistro.ItemIndex:=1;

        {  case Conexion.ExecSQLScalar('select tipo_cuenta from sic_dispersion where pago_nomina_id=:id',[nomina_id]) of
          1:cbTipoCuenta.ItemIndex:=0;
          3:cbTipoCuenta.ItemIndex:=1;
          9:cbTipoCuenta.ItemIndex:=2;
          end; }

         { case Conexion.ExecSQLScalar('select numero_banco_receptor from sic_dispersion where pago_nomina_id=:id',[nomina_id]) of
          002:cbBancoReceptor.ItemIndex:=0;
          006:cbBancoReceptor.ItemIndex:=1;
          012:cbBancoReceptor.ItemIndex:=2;
          014:cbBancoReceptor.ItemIndex:=3;
          019:cbBancoReceptor.ItemIndex:=4;
          021:cbBancoReceptor.ItemIndex:=5;
          030:cbBancoReceptor.ItemIndex:=6;
          032:cbBancoReceptor.ItemIndex:=7;
          036:cbBancoReceptor.ItemIndex:=8;
          037:cbBancoReceptor.ItemIndex:=9;
          042:cbBancoReceptor.ItemIndex:=10;
          044:cbBancoReceptor.ItemIndex:=11;
          058:cbBancoReceptor.ItemIndex:=12;
          059:cbBancoReceptor.ItemIndex:=13;
          060:cbBancoReceptor.ItemIndex:=14;
          062:cbBancoReceptor.ItemIndex:=15;
          072:cbBancoReceptor.ItemIndex:=16;
          102:cbBancoReceptor.ItemIndex:=17;
          103:cbBancoReceptor.ItemIndex:=18;
          106:cbBancoReceptor.ItemIndex:=19;
          110:cbBancoReceptor.ItemIndex:=20;
          113:cbBancoReceptor.ItemIndex:=21;
          127:cbBancoReceptor.ItemIndex:=22;
          131:cbBancoReceptor.ItemIndex:=23;
          132:cbBancoReceptor.ItemIndex:=24;
          134:cbBancoReceptor.ItemIndex:=25;
          135:cbBancoReceptor.ItemIndex:=26;
          137:cbBancoReceptor.ItemIndex:=27;
          166:cbBancoReceptor.ItemIndex:=28;
          end;  }

          dtpFechaAplicacion.Date:=Conexion.ExecSQLScalar('select fecha_aplicacion from sic_dispersion where pago_nomina_id=:id',[nomina_id]);
      end;
     end;
end;

function TFormPrincipal.obtenerSiglas (cadena : string) : string;
var
  siglas, palabra : string;
  i : integer;
begin
  siglas := '';
  palabra := '';
  for i := 1 to length(cadena) + 1 do
  begin
    if (cadena[i] <> ' ') and (i <= length(cadena)) then
        palabra := palabra + cadena[i]
    else
    begin
      if palabra <> '' then
      begin
        siglas := siglas + palabra[1];
        palabra := '';
      end;
    end;
  end;
  obtenerSiglas := siglas;
end;
end.
