object FormFechaAplicacion: TFormFechaAplicacion
  Left = 0
  Top = 0
  Caption = 'Fecha Aplicacion'
  ClientHeight = 123
  ClientWidth = 370
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 131
    Height = 16
    Caption = 'Fecha de Aplicacion:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 110
    Width = 370
    Height = 13
    Panels = <
      item
        Width = 50
      end>
  end
  object dtpFechaAplicacion: TDateTimePicker
    Left = 161
    Top = 19
    Width = 186
    Height = 21
    Date = 43777.477223576390000000
    Time = 43777.477223576390000000
    TabOrder = 1
  end
  object btnAceptar: TButton
    Left = 128
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=E:\Microsip datos 2018\AD2007.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 16
    Top = 62
  end
end
