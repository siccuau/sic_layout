program LayoutDispersion;

uses
  Vcl.Forms,
  UPrincipal in 'UPrincipal.pas' {Principal},
  ULogin in 'ULogin.pas' {Login},
  Uselempresa in 'Uselempresa.pas' {SeleccionaEmpresa},
  UConexiones in 'UConexiones.pas' {Conexiones},
  USecuencia in 'USecuencia.pas' {FormSecuencia},
  UEmpleado in 'UEmpleado.pas' {Form1},
  UFechaAplicacion in 'UFechaAplicacion.pas' {FormFechaAplicacion};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
    Application.CreateForm(TLogin, Login);
  Application.CreateForm(TConexiones, Conexiones);
  Application.CreateForm(TSeleccionaEmpresa, SeleccionaEmpresa);
  Application.CreateForm(TPrincipal, Principal);
  Application.CreateForm(TFormSecuencia, FormSecuencia);
  Application.CreateForm(TFormEmpleado, FormEmpleado);
  Application.CreateForm(TFormFechaAplicacion, FormFechaAplicacion);
  Application.Run;
end.
