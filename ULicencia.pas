unit ULicencia;

interface

uses
    {WbemScripting_TLB,}
    system.Variants,Registry,System.SysUtils,UEnvVars,Windows, Vcl.Dialogs, ActiveX;

type TLicencia = class

    private
    public
    {Function GetMotherBoardSerial:string;
    Function GetPCModel:string;}
    function Decrypt (const s: string; Key: Word) : string;
    function Encrypt (const s: string; Key: Word) : string;
    function StringToWord(const st:string):word;
    function SetGlobalEnvironment(const Name, Value: string;  const User: Boolean = True): Boolean;
    function GetEnvVariable(Name: string; User: Boolean = True): string;

    end;


implementation

{
Function  TLicencia.GetMotherBoardSerial:string;
var
  WMIServices : ISWbemServices;
  Root        : ISWbemObjectSet;
  Item        : Variant;
  i: Integer;
  SWbemObjectSet  : ISWbemObjectSet;

  SObject         : ISWbemObject;
  Enum            : IEnumVariant;
  TempObj         : OleVariant;
  Value           : Cardinal;
  SWbemPropertySet: ISWbemPropertySet;
begin

  WMIServices := CoSWbemLocator.Create.ConnectServer('.', 'root\cimv2','', '', '', '', 0, nil);
  //Root  := WMIServices.ExecQuery('Select SerialNumber From Win32_BaseBoard','WQL', 0, nil);
  Root  := WMIServices.ExecQuery('SELECT SerialNumber FROM Win32_PhysicalMedia','WQL', 0, nil);
  //showmessage(inttostr(root.count));
  for i := 0 to (Root.Count -1)  do
  begin
    Item := Root.ItemIndex(i);
    if Item.SerialNumber <> null
    then Result:=VarToStr(Item.SerialNumber);
  end;
end;

Function  TLicencia.GetPCModel:string;
var
  WMIServices : ISWbemServices;
  Root        : ISWbemObjectSet;
  Item        : Variant;
  i: Integer;
begin

  WMIServices := CoSWbemLocator.Create.ConnectServer('.', 'root\cimv2','', '', '', '', 0, nil);
  //Root  := WMIServices.ExecQuery('Select SerialNumber From Win32_BaseBoard','WQL', 0, nil);
  Root  := WMIServices.ExecQuery('SELECT model FROM Win32_computersystem','WQL', 0, nil);
  //showmessage(inttostr(root.count));
  for i := 0 to (Root.Count -1)  do
  begin
    Item := Root.ItemIndex(i);
    if Item.SerialNumber <> null
    then Result:=VarToStr(Item.SerialNumber);
  end;
end;

}


const
  c1 = 52845;
  c2 = 22719;

function TLicencia.Encrypt (const s: string; Key: Word) : string;
var
  i : byte;
begin
  //  Result[0] := s[0];
  SetLength(Result, length(s));
  for i := 1 to length (s) do
    begin
      Result[i] := Char (byte (s[i]) xor (Key shr 8));
      Key := (byte (Result[i]) + Key) * c1 + c2
    end
end;


function TLicencia.Decrypt (const s: string; Key: Word) : string;
var
  i : byte;
begin
  //Result[0] := s[0];
  SetLength(Result, length(s));
  for i := 1 to length (s) do
    begin
      Result[i] := Char (byte (s[i]) xor (Key shr 8));
      Key := (byte (s[i]) + Key) * c1 + c2
    end
end;

function TLicencia.StringToWord(const st:string):word;
var
  i:integer;
begin
  Result:= 0;
  for i := 0 to length(st)+1 do
    begin
       Result := Result + ord(st[i]);
    end;
end;


function TLicencia.SetGlobalEnvironment(const Name, Value: string;  const User: Boolean = True): Boolean;
resourcestring
  REG_MACHINE_LOCATION = 'System\CurrentControlSet\Control\Session Manager\Environment';
  REG_USER_LOCATION = 'Environment';
  var
  env : TEnvVar;
begin
env := TEnvVar.create;
  with TRegistry.Create do
    try
      Result := OpenKey(REG_USER_LOCATION, True);
      if Result then
      begin
        WriteString(Name, Value); { Write Registry for Global Environment }
        { Update Current Process Environment Variable }
        env.SetEnvVarValue(PChar(Name), PChar(Value));
        { Send Message To All Top Window for Refresh }
        //SendMessage(HWND_BROADCAST, WM_SETTINGCHANGE, 0, Integer(PChar('Environment')));
      end;
    finally
      Free;
    end;
end;

function TLicencia.GetEnvVariable(Name: string; User: Boolean = True): string;
var
  Str: array[0..255] of char;
begin
  with TRegistry.Create do
  try
//    if User then
    begin
      RootKey := HKEY_CURRENT_USER;
      //OpenKey('Environment', False);
      OpenKeyReadOnly('Environment');
    end;
//    else
//    begin
//      RootKey := HKEY_LOCAL_MACHINE;
//      //OpenKey('SYSTEM\CurrentControlSet\Control\Session ' +
//      //  'Manager\Environment', False);
//      OpenKeyReadOnly('SYSTEM\CurrentControlSet\Control\Session ' +
//        'Manager\Environment', False);
//    end;
    Result := ReadString(Name);
    ExpandEnvironmentStrings(PChar(Result), Str, 255);
    Result := Str;
  finally
    Free;
  end;
end;


end.
