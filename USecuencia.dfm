object FormSecuencia: TFormSecuencia
  Left = 0
  Top = 0
  Caption = 'Secuencia'
  ClientHeight = 111
  ClientWidth = 298
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 69
    Top = 8
    Width = 70
    Height = 16
    Caption = 'Secuencia:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 8
    Top = 39
    Width = 131
    Height = 16
    Caption = 'Fecha de Aplicacion:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 98
    Width = 298
    Height = 13
    Panels = <
      item
        Width = 50
      end>
  end
  object cbSecuencia: TComboBox
    Left = 145
    Top = 8
    Width = 37
    Height = 21
    ItemIndex = 0
    TabOrder = 1
    Text = '01'
    Items.Strings = (
      '01'
      '02'
      '03'
      '04'
      '05'
      '06'
      '07'
      '08'
      '09'
      '10'
      '11'
      '12'
      '13'
      '14'
      '15'
      '16'
      '17'
      '18'
      '19'
      '20'
      '21'
      '22'
      '23'
      '24'
      '25'
      '26'
      '27'
      '28'
      '29'
      '30')
  end
  object btnAceptar: TButton
    Left = 84
    Top = 67
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object dtpFechaAplicacion: TDateTimePicker
    Left = 145
    Top = 38
    Width = 139
    Height = 21
    Date = 43777.397405578700000000
    Time = 43777.397405578700000000
    TabOrder = 3
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=E:\Microsip datos 2018\AD2007.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 8
    Top = 190
  end
end
