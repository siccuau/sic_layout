unit USecuencia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client;

type
  TFormSecuencia = class(TForm)
    Label1: TLabel;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    cbSecuencia: TComboBox;
    btnAceptar: TButton;
    Label8: TLabel;
    dtpFechaAplicacion: TDateTimePicker;
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    secuencia,fecha,rfc,fecha2:string;
  end;

var
  FormSecuencia: TFormSecuencia;

implementation

{$R *.dfm}

procedure TFormSecuencia.btnAceptarClick(Sender: TObject);
begin
Conexion.ExecSQL('update sic_dispersion set secuencia=:sec where rfc=:id',[cbSecuencia.Text,rfc]);
secuencia:=cbSecuencia.Text;
fecha:=formatdatetime('mm/dd/yyyy',dtpFechaAplicacion.Date);
fecha2:=formatdatetime('ddmmyyyy',dtpFechaAplicacion.Date);
self.Close;
end;

end.
