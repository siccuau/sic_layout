object Principal: TPrincipal
  Left = 0
  Top = 0
  Caption = 'Principal'
  ClientHeight = 588
  ClientWidth = 931
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 575
    Width = 931
    Height = 13
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 371
  end
  object PageControl1: TPageControl
    Left = 0
    Top = -1
    Width = 923
    Height = 570
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Scotiabank Inverlat'
      ExplicitHeight = 342
      object Label1: TLabel
        Left = 23
        Top = 10
        Width = 19
        Height = 16
        Caption = 'del'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 183
        Top = 10
        Width = 11
        Height = 16
        Caption = 'al'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object gridDatos: TDBGrid
        Left = 3
        Top = 64
        Width = 909
        Height = 425
        DataSource = dsDatos
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = gridDatosDrawColumnCell
      end
      object dtpFechaIni: TDateTimePicker
        Left = 48
        Top = 5
        Width = 121
        Height = 21
        Date = 43773.638311180560000000
        Time = 43773.638311180560000000
        TabOrder = 1
      end
      object dtpFechaFin: TDateTimePicker
        Left = 208
        Top = 5
        Width = 121
        Height = 21
        Date = 43773.638311180560000000
        Time = 43773.638311180560000000
        TabOrder = 2
      end
      object btnBuscar: TButton
        Left = 335
        Top = 1
        Width = 75
        Height = 25
        Caption = 'Buscar'
        TabOrder = 3
        OnClick = btnBuscarClick
      end
      object btnGenerar: TButton
        Left = 360
        Top = 495
        Width = 169
        Height = 34
        Caption = 'Generar Layout Scotiabank'
        TabOrder = 4
        OnClick = btnGenerarClick
      end
      object CheckBox1: TCheckBox
        Left = 432
        Top = 9
        Width = 97
        Height = 17
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 5
        Visible = False
        OnClick = CheckBox1Click
      end
      object cbAll: TCheckBox
        Left = 8
        Top = 41
        Width = 97
        Height = 17
        Caption = 'Seleccionar Todo'
        Checked = True
        Ctl3D = True
        Enabled = False
        ParentCtl3D = False
        State = cbChecked
        TabOrder = 6
        OnClick = cbAllClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Santander'
      ImageIndex = 1
      ExplicitHeight = 338
      object Label3: TLabel
        Left = 31
        Top = 18
        Width = 19
        Height = 16
        Caption = 'del'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 191
        Top = 18
        Width = 11
        Height = 16
        Caption = 'al'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DateTimePicker1: TDateTimePicker
        Left = 56
        Top = 13
        Width = 121
        Height = 21
        Date = 43773.638311180560000000
        Time = 43773.638311180560000000
        TabOrder = 0
      end
      object DateTimePicker2: TDateTimePicker
        Left = 216
        Top = 13
        Width = 121
        Height = 21
        Date = 43773.638311180560000000
        Time = 43773.638311180560000000
        TabOrder = 1
      end
      object btnBuscarSantander: TButton
        Left = 343
        Top = 9
        Width = 75
        Height = 25
        Caption = 'Buscar'
        TabOrder = 2
        OnClick = btnBuscarSantanderClick
      end
      object cbAllSantander: TCheckBox
        Left = 16
        Top = 49
        Width = 97
        Height = 17
        Caption = 'Seleccionar Todo'
        Checked = True
        Ctl3D = True
        Enabled = False
        ParentCtl3D = False
        State = cbChecked
        TabOrder = 4
        OnClick = cbAllSantanderClick
      end
      object gridDatosSantander: TDBGrid
        Left = 3
        Top = 72
        Width = 909
        Height = 417
        DataSource = dsDatosSantander
        TabOrder = 5
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = gridDatosSantanderDrawColumnCell
        OnDblClick = gridDatosSantanderDblClick
      end
      object btnGenerarSantander: TButton
        Left = 368
        Top = 507
        Width = 169
        Height = 32
        Caption = 'Generar Layout Santander'
        TabOrder = 6
        OnClick = btnGenerarSantanderClick
      end
      object CheckBox2: TCheckBox
        Left = 440
        Top = 17
        Width = 97
        Height = 17
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 3
        Visible = False
        WordWrap = True
        OnClick = CheckBox2Click
      end
    end
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=E:\Microsip datos 2018\AD2000.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 32
    Top = 240
  end
  object qryDatos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select '#39'S'#39' as entregado,pn.pago_nomina_id,pn.total_percep,pn.tot' +
        'al_reten,pn.fecha,e.rfc,e.nombres,e.apellido_paterno,e.apellido_' +
        'materno,e.num_ctaban_pago_elect,e.email,cb.num_cuenta,e.numero f' +
        'rom pagos_nomina pn'
      'join empleados e on pn.empleado_id=e.empleado_id'
      
        'join grupos_pagos_elect gpe on gpe.grupo_pago_elect_id=e.grupo_p' +
        'ago_elect_id'
      'join cuentas_bancarias cb on cb.cuenta_ban_id=gpe.cuenta_ban_id'
      'where pn.fecha between '#39'04/11/2010'#39' and '#39'04/11/2019'#39
      'ORDER BY e.APELLIDO_PATERNO,e.APELLIDO_MATERNO,e.NOMBRES')
    Left = 96
    Top = 240
    object qryDatosENTREGADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = ' '
      DisplayWidth = 2
      FieldName = 'ENTREGADO'
      Origin = 'ENTREGADO'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object qryDatosNOMBRES: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 15
      FieldName = 'NOMBRES'
      Origin = 'NOMBRES'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosAPELLIDO_PATERNO: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 15
      FieldName = 'APELLIDO_PATERNO'
      Origin = 'APELLIDO_PATERNO'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosAPELLIDO_MATERNO: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 15
      FieldName = 'APELLIDO_MATERNO'
      Origin = 'APELLIDO_MATERNO'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosRFC: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 13
      FieldName = 'RFC'
      Origin = 'RFC'
      ProviderFlags = []
      ReadOnly = True
      Size = 18
    end
    object qryDatosFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
      Required = True
    end
    object qryDatosPAGO_NOMINA_ID: TIntegerField
      FieldName = 'PAGO_NOMINA_ID'
      Origin = 'PAGO_NOMINA_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryDatosTOTAL_PERCEP: TBCDField
      FieldName = 'TOTAL_PERCEP'
      Origin = 'TOTAL_PERCEP'
      Required = True
      Precision = 18
      Size = 2
    end
    object qryDatosTOTAL_RETEN: TBCDField
      FieldName = 'TOTAL_RETEN'
      Origin = 'TOTAL_RETEN'
      Required = True
      Precision = 18
      Size = 2
    end
    object qryDatosNUM_CTABAN_PAGO_ELECT: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NUM_CTABAN_PAGO_ELECT'
      Origin = 'NUM_CTABAN_PAGO_ELECT'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosEMAIL: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryDatosNUM_CUENTA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NUM_CUENTA'
      Origin = 'NUM_CUENTA'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosNUMERO: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object dsDatos: TDataSource
    DataSet = qryDatos
    Left = 144
    Top = 240
  end
  object qryNomina: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_dispersion where rfc=:id')
    Left = 192
    Top = 240
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryNominaRFC: TStringField
      FieldName = 'RFC'
      Origin = 'RFC'
      Required = True
    end
    object qryNominaNUMERO_CONTRATO: TStringField
      FieldName = 'NUMERO_CONTRATO'
      Origin = 'NUMERO_CONTRATO'
      Size = 5
    end
    object qryNominaSECUENCIA: TStringField
      FieldName = 'SECUENCIA'
      Origin = 'SECUENCIA'
      Size = 2
    end
    object qryNominaREFERENCIA_EMPRESA: TStringField
      FieldName = 'REFERENCIA_EMPRESA'
      Origin = 'REFERENCIA_EMPRESA'
      Size = 10
    end
    object qryNominaCLAVE_BENEFICIARIO: TStringField
      FieldName = 'CLAVE_BENEFICIARIO'
      Origin = 'CLAVE_BENEFICIARIO'
    end
    object qryNominaTIPO_CUENTA: TStringField
      FieldName = 'TIPO_CUENTA'
      Origin = 'TIPO_CUENTA'
      Size = 1
    end
    object qryNominaNUMERO_BANCO_RECEPTOR: TStringField
      FieldName = 'NUMERO_BANCO_RECEPTOR'
      Origin = 'NUMERO_BANCO_RECEPTOR'
      Size = 3
    end
    object qryNominaTIPO_REGISTRO: TStringField
      FieldName = 'TIPO_REGISTRO'
      Origin = 'TIPO_REGISTRO'
      Size = 2
    end
    object qryNominaNUM_EMPLEADO: TStringField
      FieldName = 'NUM_EMPLEADO'
      Origin = 'NUM_EMPLEADO'
      Size = 7
    end
    object qryNominaFECHA_APLICACION: TDateField
      FieldName = 'FECHA_APLICACION'
      Origin = 'FECHA_APLICACION'
    end
  end
  object qryDatosSantander: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select '#39'S'#39' as entregado,pn.pago_nomina_id,pn.total_percep,pn.tot' +
        'al_reten,pn.fecha,e.rfc,e.nombres,e.apellido_paterno,e.apellido_' +
        'materno,e.num_ctaban_pago_elect,e.email,cb.num_cuenta from pagos' +
        '_nomina pn'
      'join empleados e on pn.empleado_id=e.empleado_id'
      
        'join grupos_pagos_elect gpe on gpe.grupo_pago_elect_id=e.grupo_p' +
        'ago_elect_id'
      'join cuentas_bancarias cb on cb.cuenta_ban_id=gpe.cuenta_ban_id'
      
        'where pn.fecha between :fecha_ini and :fecha_fin and gpe.nombre=' +
        #39'Santander'#39)
    Left = 88
    Top = 304
    ParamData = <
      item
        Name = 'FECHA_INI'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'FECHA_FIN'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryDatosSantanderENTREGADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = ' '
      DisplayWidth = 2
      FieldName = 'ENTREGADO'
      Origin = 'ENTREGADO'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object qryDatosSantanderNOMBRES: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 15
      FieldName = 'NOMBRES'
      Origin = 'NOMBRES'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosSantanderAPELLIDO_PATERNO: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 15
      FieldName = 'APELLIDO_PATERNO'
      Origin = 'APELLIDO_PATERNO'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosSantanderAPELLIDO_MATERNO: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 15
      FieldName = 'APELLIDO_MATERNO'
      Origin = 'APELLIDO_MATERNO'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosSantanderRFC: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 13
      FieldName = 'RFC'
      Origin = 'RFC'
      ProviderFlags = []
      ReadOnly = True
      Size = 18
    end
    object qryDatosSantanderFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
      Required = True
    end
    object qryDatosSantanderPAGO_NOMINA_ID: TIntegerField
      FieldName = 'PAGO_NOMINA_ID'
      Origin = 'PAGO_NOMINA_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryDatosSantanderTOTAL_PERCEP: TBCDField
      FieldName = 'TOTAL_PERCEP'
      Origin = 'TOTAL_PERCEP'
      Required = True
      Precision = 18
      Size = 2
    end
    object qryDatosSantanderTOTAL_RETEN: TBCDField
      FieldName = 'TOTAL_RETEN'
      Origin = 'TOTAL_RETEN'
      Required = True
      Precision = 18
      Size = 2
    end
    object qryDatosSantanderNUM_CTABAN_PAGO_ELECT: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NUM_CTABAN_PAGO_ELECT'
      Origin = 'NUM_CTABAN_PAGO_ELECT'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryDatosSantanderEMAIL: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryDatosSantanderNUM_CUENTA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NUM_CUENTA'
      Origin = 'NUM_CUENTA'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
  end
  object dsDatosSantander: TDataSource
    DataSet = qryDatosSantander
    Left = 136
    Top = 304
  end
  object qryEmpleado: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select numero,apellido_paterno,apellido_materno,nombres from emp' +
        'leados e'
      
        'join pagos_nomina pn on pn.empleado_id=e.empleado_id where pn.pa' +
        'go_nomina_id=:pid')
    Left = 16
    Top = 304
    ParamData = <
      item
        Name = 'PID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryEmpleadoNUMERO: TIntegerField
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
      Required = True
    end
    object qryEmpleadoAPELLIDO_PATERNO: TStringField
      FieldName = 'APELLIDO_PATERNO'
      Origin = 'APELLIDO_PATERNO'
      Required = True
      Size = 30
    end
    object qryEmpleadoAPELLIDO_MATERNO: TStringField
      FieldName = 'APELLIDO_MATERNO'
      Origin = 'APELLIDO_MATERNO'
      Size = 30
    end
    object qryEmpleadoNOMBRES: TStringField
      FieldName = 'NOMBRES'
      Origin = 'NOMBRES'
      Required = True
      Size = 30
    end
  end
end
