unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Vcl.StdCtrls, FireDAC.Comp.DataSet, Vcl.Grids, Vcl.DBGrids,ULayout,USecuencia,UEmpleado,UFechaAplicacion;

type
  TPrincipal = class(TForm)
    StatusBar1: TStatusBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    gridDatos: TDBGrid;
    dtpFechaIni: TDateTimePicker;
    dtpFechaFin: TDateTimePicker;
    btnBuscar: TButton;
    btnGenerar: TButton;
    CheckBox1: TCheckBox;
    cbAll: TCheckBox;
    Conexion: TFDConnection;
    qryDatos: TFDQuery;
    qryDatosENTREGADO: TStringField;
    qryDatosPAGO_NOMINA_ID: TIntegerField;
    qryDatosTOTAL_PERCEP: TBCDField;
    qryDatosTOTAL_RETEN: TBCDField;
    qryDatosFECHA: TDateField;
    qryDatosRFC: TStringField;
    qryDatosNOMBRES: TStringField;
    qryDatosAPELLIDO_PATERNO: TStringField;
    qryDatosAPELLIDO_MATERNO: TStringField;
    qryDatosNUM_CTABAN_PAGO_ELECT: TStringField;
    qryDatosEMAIL: TStringField;
    dsDatos: TDataSource;
    qryNomina: TFDQuery;
    TabSheet2: TTabSheet;
    Label3: TLabel;
    DateTimePicker1: TDateTimePicker;
    Label4: TLabel;
    DateTimePicker2: TDateTimePicker;
    btnBuscarSantander: TButton;
    CheckBox2: TCheckBox;
    cbAllSantander: TCheckBox;
    gridDatosSantander: TDBGrid;
    btnGenerarSantander: TButton;
    qryDatosSantander: TFDQuery;
    dsDatosSantander: TDataSource;
    qryDatosSantanderENTREGADO: TStringField;
    qryDatosSantanderPAGO_NOMINA_ID: TIntegerField;
    qryDatosSantanderTOTAL_PERCEP: TBCDField;
    qryDatosSantanderTOTAL_RETEN: TBCDField;
    qryDatosSantanderFECHA: TDateField;
    qryDatosSantanderRFC: TStringField;
    qryDatosSantanderNOMBRES: TStringField;
    qryDatosSantanderAPELLIDO_PATERNO: TStringField;
    qryDatosSantanderAPELLIDO_MATERNO: TStringField;
    qryDatosSantanderNUM_CTABAN_PAGO_ELECT: TStringField;
    qryDatosSantanderEMAIL: TStringField;
    qryDatosSantanderNUM_CUENTA: TStringField;
    qryDatosNUM_CUENTA: TStringField;
    qryDatosNUMERO: TIntegerField;
    qryEmpleado: TFDQuery;
    qryEmpleadoNUMERO: TIntegerField;
    qryEmpleadoAPELLIDO_PATERNO: TStringField;
    qryEmpleadoAPELLIDO_MATERNO: TStringField;
    qryEmpleadoNOMBRES: TStringField;
    qryNominaRFC: TStringField;
    qryNominaNUMERO_CONTRATO: TStringField;
    qryNominaSECUENCIA: TStringField;
    qryNominaREFERENCIA_EMPRESA: TStringField;
    qryNominaCLAVE_BENEFICIARIO: TStringField;
    qryNominaTIPO_CUENTA: TStringField;
    qryNominaNUMERO_BANCO_RECEPTOR: TStringField;
    qryNominaTIPO_REGISTRO: TStringField;
    qryNominaNUM_EMPLEADO: TStringField;
    qryNominaFECHA_APLICACION: TDateField;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure crearArchivoTexto;
        procedure crearArchivoTextoSantander;
    procedure btnGenerarClick(Sender: TObject);
    procedure gridDatosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CheckBox1Click(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure btnBuscarSantanderClick(Sender: TObject);
    procedure btnGenerarSantanderClick(Sender: TObject);
    procedure gridDatosSantanderDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure CheckBox2Click(Sender: TObject);
    procedure cbAllSantanderClick(Sender: TObject);
    procedure gridDatosSantanderDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    function obtenerSiglas (cadena : string) : string;
  private
    { Private declarations }
  public
    secuencia,fecha1,fecha2:string;
    fecha_aplicacion:string;
  end;

var
  Principal: TPrincipal;

implementation

{$R *.dfm}

procedure TPrincipal.btnBuscarClick(Sender: TObject);
var
fechaini,fechafin:String;
begin
fechaini:=formatdatetime('mm/dd/yyyy',dtpFechaIni.DateTime);
fechafin:=formatdatetime('mm/dd/yyyy',dtpFechaFin.DateTime);
qryDatos.SQL.Text:='select ''S'' as entregado,pn.pago_nomina_id,pn.total_percep,pn.total_reten,pn.fecha,e.rfc,e.nombres,e.apellido_paterno,'+
                    'e.apellido_materno,e.num_ctaban_pago_elect,e.email,cb.num_cuenta,e.numero from pagos_nomina pn'+
                   ' join empleados e on pn.empleado_id=e.empleado_id'+
                  ' join grupos_pagos_elect gpe on gpe.grupo_pago_elect_id=e.grupo_pago_elect_id'+
                  ' join cuentas_bancarias cb on cb.cuenta_ban_id=gpe.cuenta_ban_id'+
                   ' where pn.fecha between :fecha_ini and :fecha_fin ORDER BY e.APELLIDO_PATERNO,e.APELLIDO_MATERNO,e.NOMBRES';
qryDatos.ParamByName('fecha_ini').Value:=fechaini;
qryDatos.ParamByName('fecha_fin').Value:=fechafin;
qryDatos.Open;
cbAll.Enabled:=true;
end;



procedure TPrincipal.gridDatosDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
  const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
  var
 DrawState: Integer;
 DrawRect: TRect;
begin
 if Column.FieldName.ToUpper = 'ENTREGADO' then
  begin
    if (gdSelected in State) and (qryDatos.RecordCount > 0) then
    begin
      with CheckBox1 do
      begin
        Left := Rect.Left + gridDatos.Left + 2;
        Top := Rect.Top + gridDatos.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := gridDatos.Canvas.Brush.Color;
        Visible := true;
        Checked := qryDatosENTREGADO.Value = 'S';
      end;
    end
    else
    begin
      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[Column.Field.AsString = 'S'];
     gridDatos.Canvas.FillRect(Rect);
     DrawFrameControl(gridDatos.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;
end;

procedure TPrincipal.gridDatosSantanderDblClick(Sender: TObject);
var
FormEmpleado:TFormEmpleado;
begin
  FormEmpleado:=TFormEmpleado.Create(nil);
  FormEmpleado.Conexion.Params := Conexion.Params;
  FormEmpleado.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormEmpleado.rfc:=qryDatosSantanderRFC.Value;
 FormEmpleado.ShowModal;
 FormEmpleado.Free;
end;


procedure TPrincipal.gridDatosSantanderDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
 const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
  var
 DrawState: Integer;
 DrawRect: TRect;
begin
 if Column.FieldName.ToUpper = 'ENTREGADO' then
  begin
    if (gdSelected in State) and (qryDatosSantander.RecordCount > 0) then
    begin
      with CheckBox2 do
      begin
        Left := Rect.Left + gridDatosSantander.Left+2;
        Top := Rect.Top + gridDatosSantander.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := gridDatosSantander.Canvas.Brush.Color;
        Visible := true;
        Checked := qryDatosSantanderENTREGADO.Value = 'S';
      end;
    end
    else
    begin
      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[Column.Field.AsString = 'S'];
     gridDatosSantander.Canvas.FillRect(Rect);
     DrawFrameControl(gridDatosSantander.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;

end;

procedure TPrincipal.btnBuscarSantanderClick(Sender: TObject);
var
fechaini,fechafin:String;
begin
fechaini:=formatdatetime('dd/mm/yyyy',DateTimePicker1.DateTime);
fechafin:=formatdatetime('dd/mm/yyyy',DateTimePicker2.DateTime);
qryDatosSantander.SQL.Text:='select ''S'' as entregado,pn.pago_nomina_id,pn.total_percep,pn.total_reten,pn.fecha,e.rfc,e.nombres,e.apellido_paterno,'+
                    'e.apellido_materno,e.num_ctaban_pago_elect,e.email,cb.num_cuenta from pagos_nomina pn'+
                   ' join empleados e on pn.empleado_id=e.empleado_id'+
                    ' join grupos_pagos_elect gpe on gpe.grupo_pago_elect_id=e.grupo_pago_elect_id'+
                  ' join cuentas_bancarias cb on cb.cuenta_ban_id=gpe.cuenta_ban_id'+
                   ' where pn.fecha between :fecha_ini and :fecha_fin and (upper(gpe.nombre)=''OPERATIVOS SANTANDER'' or upper(gpe.nombre)='' ADMINISTRATIVOS SANTANDER'') ORDER BY e.APELLIDO_PATERNO,e.APELLIDO_MATERNO,e.NOMBRES';
qryDatosSantander.ParamByName('fecha_ini').Value:=fechaini;
qryDatosSantander.ParamByName('fecha_fin').Value:=fechafin;
qryDatosSantander.Open;
cbAllSantander.Enabled:=true;
end;

procedure TPrincipal.btnGenerarClick(Sender: TObject);
var
 FormSecuencia:TFormSecuencia;
begin
 FormSecuencia:=TFormSecuencia.Create(nil);
 FormSecuencia.Conexion.Params := Conexion.Params;
 FormSecuencia.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
 FormSecuencia.rfc:=qryDatosRFC.Value;
 FormSecuencia.ShowModal;
 secuencia:=FormSecuencia.secuencia;
 fecha1:=FormSecuencia.fecha;
 fecha2:=FormSecuencia.fecha2;
 {generar layout de los seleccionados}
 crearArchivoTexto;
end;

procedure TPrincipal.btnGenerarSantanderClick(Sender: TObject);
var
 FormFechaAplicacion:TFormFechaAplicacion;
begin
 FormFechaAplicacion:=TFormFechaAplicacion.Create(nil);
 FormFechaAplicacion.Conexion.Params := Conexion.Params;
 FormFechaAplicacion.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
 FormFechaAplicacion.ShowModal;
 fecha_aplicacion:=FormFechaAplicacion.fecha_aplicacion;
crearArchivoTextoSantander;
end;

procedure TPrincipal.cbAllClick(Sender: TObject);
begin
  qryDatos.DisableControls;
  qryDatos.First;
  while not qryDatos.Eof do
  begin
    if cbAll.Checked then
    begin
    qryDatos.Edit;
    qryDatosENTREGADO.Value := 'S';
    qryDatos.Post;
    qryDatos.Next;
   end
     else
       begin
        qryDatos.Edit;
        qryDatosENTREGADO.Value := 'N';
        qryDatos.Post;
        qryDatos.Next;
       end;
  end;
  qryDatos.First;
  qryDatos.EnableControls;
  CheckBox1.Checked := qryDatosENTREGADO.Value = 'S';
end;

procedure TPrincipal.cbAllSantanderClick(Sender: TObject);
begin
 qryDatosSantander.DisableControls;
  qryDatosSantander.First;
  while not qryDatosSantander.Eof do
  begin
    if cbAllSantander.Checked then
    begin
    qryDatosSantander.Edit;
    qryDatosSantanderENTREGADO.Value := 'S';
    qryDatosSantander.Post;
    qryDatosSantander.Next;
   end
     else
       begin
        qryDatosSantander.Edit;
        qryDatosSantanderENTREGADO.Value := 'N';
        qryDatosSantander.Post;
        qryDatosSantander.Next;
       end;
  end;
  qryDatosSantander.First;
  qryDatosSantander.EnableControls;
  CheckBox2.Checked := qryDatosSantanderENTREGADO.Value = 'S';
end;

procedure TPrincipal.CheckBox1Click(Sender: TObject);
begin
 qryDatos.Edit;
  if CheckBox1.Checked then
  begin
  qryDatosENTREGADO.Value := 'S';
  end
    else
  qryDatosENTREGADO.Value := 'N';
  qryDatos.Post;
end;

procedure TPrincipal.CheckBox2Click(Sender: TObject);
begin
 qryDatosSantander.Edit;
  if CheckBox2.Checked then
  begin
  qryDatosSantanderENTREGADO.Value := 'S';
  end
    else
  qryDatosSantanderENTREGADO.Value := 'N';
  qryDatosSantander.Post;
end;

procedure TPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Conexion.Close;
Application.Terminate;
end;

procedure TPrincipal.FormShow(Sender: TObject);
begin
//CREAR TABLA
 //TABLA SIC_DISPERSION
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_DISPERSION'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_DISPERSION ('+
                              'RFC VARCHAR(20) NOT NULL,'+
                              'NUMERO_CONTRATO VARCHAR(5),'+
                              'SECUENCIA VARCHAR(2),'+
                              'REFERENCIA_EMPRESA VARCHAR(10),'+
                              'CLAVE_BENEFICIARIO VARCHAR(20),'+
                              'TIPO_CUENTA VARCHAR(1),'+
                              'NUMERO_BANCO_RECEPTOR VARCHAR(3),'+
                              'TIPO_REGISTRO VARCHAR(2),'+
                              'NUM_EMPLEADO VARCHAR(7),'+
                              'FECHA_APLICACION DATE'+
                              ');'';'+
                               ' END end');
        {  Conexion.ExecSQL('execute block as begin if (exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_DISPERSION'')) then'+
                             ' BEGIN'+
                             ' execute statement ''DROP TABLE SIC_DISPERSION;'';'+
                               ' END end');}
end;

procedure TPrincipal.crearArchivoTexto;
var
importe,nombre_beneficiario,cuenta_beneficiario,concepto_pago,email,tipo_registro:string;
tipocuenta,num_banco_receptor,fecha,rfc,fechareferencia,nombre,clave_beneficiario:string;
importe_DA_total,conteo_DA_final,importe_DB_total,conteo_DB_final:string;
conteo_DA,conteo_DB,secuencia1:integer;
F:TextFile;
begin
secuencia1:=0;
//ABRIR CONSULTA
qryNomina.SQL.Text:='select * from sic_dispersion where rfc=:rfc';
qryNomina.ParamByName('rfc').Value:=qryDatosRFC.Value;
qryNomina.Open;

//CREAR EL TXT
AssignFile( F, ExtractFilePath( Application.ExeName ) + 'Layout Scotiabank '+secuencia+'.txt' );
  Rewrite( F );
  //ENCABEZADO*******************************************************
  WriteLn( F, 'EEHA'+{num de contrato}'04871'+qryNominaSECUENCIA.Value+StringOfChar('0',27)+StringOfChar(' ',332));
  WriteLn( F,'EEHB00'+StringOfChar('0',4)+'25600634189'+
  StringOfChar(' ',11-Length('25600634189'))+StringOfChar('0',2)+fecha2+StringOfChar('0',3)+StringOfChar(' ',336));
 //**************************************DETALLADO//**********************************************************************
 conteo_DA:=0;
 importe_DA_total:='0';
  conteo_DB:=0;
 importe_DB_total:='0';
 //try


 //RECORRER GRID
 qryDatos.First;
 while not qryDatos.Eof do
      begin
       if qryDatosENTREGADO.Value='S' then
       begin
       //CONSULTA PARA DATOS DEL EMPLEADO
          qryEmpleado.SQL.Text:='select numero,apellido_paterno,apellido_materno,nombres from empleados e'+
          ' join pagos_nomina pn on pn.empleado_id=e.empleado_id where e.rfc=:rfc';
          qryEmpleado.ParamByName('rfc').Value:=qryDatosRFC.Value;
          qryEmpleado.Open;
          //////////////////////////

            if Conexion.ExecSQLScalar('select count(rfc) from sic_dispersion where rfc=:rfc',[qryDatosRFC.Value])>0 then
               begin
               //VARIABLES
               fechareferencia:=formatdatetime('ddmmyyyy',Now);
                nombre:=qryEmpleadoNOMBRES.Value+' '+qryEmpleadoAPELLIDO_PATERNO.Value+' '+qryEmpleadoAPELLIDO_MATERNO.Value;
                clave_beneficiario:=uppercase(obtenerSiglas(nombre))+inttostr(qryEmpleadoNUMERO.Value);
               //CONSULTA
                   Conexion.ExecSQL('update sic_dispersion set referencia_empresa=:ref,'+
                                'clave_beneficiario=:cve,tipo_cuenta=:tipo,numero_banco_receptor=:num,tipo_registro=:tip,fecha_aplicacion=:fecha where rfc'+
                   '=:pago',['00'+fechareferencia,clave_beneficiario+StringOfChar(' ',20-Length(clave_beneficiario)),'1','044','DA',fecha1,qryDatosRFC.Value]);
               end
               else
               begin
               Conexion.ExecSQL('insert into sic_dispersion(rfc,secuencia,referencia_empresa,clave_beneficiario,tipo_cuenta,numero_banco_receptor,tipo_registro,num_empleado,fecha_aplicacion) values(:pago,:sec,:ref,:cve,:tipo,:num,:tip,:emp,:fecha)',[qryDatosRFC.Value,'01','00'+fechareferencia,clave_beneficiario+StringOfChar(' ',20-Length(clave_beneficiario)),'1','044','DA',NULL,fecha1]);
               end;

        if (Conexion.ExecSQLScalar('select count(rfc) from sic_dispersion where rfc=:id',[qryDatosRFC.Value]))=0 then
          begin
           ShowMessage('El cliente con el rfc: '+qryDatosRFC.Value+' no contiene datos.');
           qryNomina.Last;
           break;
          end
          else
        begin
       qryNomina.SQL.Text:='select * from sic_dispersion where rfc=:id';
       qryNomina.ParamByName('id').Value:=qryDatosRFC.Value;
       qryNomina.Open;
         //VARIABLES
       tipo_registro:=qryNominaTIPO_REGISTRO.Value;
        importe:=Floattostr((qryDatosTOTAL_PERCEP.Value)-(qryDatosTOTAL_RETEN.Value));
        if tipo_registro='DA' then
        begin
        importe_DA_total:=floattostr(strtofloat(importe_DA_total)+strtofloat(importe));
        importe_DA_total:=Format('%.2f',[strtofloat(importe_DA_total)]);
        conteo_DA:=conteo_DA+1;
        end
        else
        begin
        importe_DB_total:=floattostr(strtofloat(importe_DB_total)+strtofloat(importe));
        conteo_DB:=conteo_DB+1;
        end;
        importe:=Format('%.2f', [strtofloat(importe)]);
        importe:=importe.replace('.','');
        importe:=Format('%.*d',[15,strtoint64(importe)]);
        nombre_beneficiario:=uppercase(qryDatosAPELLIDO_PATERNO.Value)+' '+uppercase(qryDatosAPELLIDO_MATERNO.Value)+' '+uppercase(qryDatosNOMBRES.Value);
        nombre_beneficiario:=nombre_beneficiario+StringOfChar(' ',40-Length(nombre_beneficiario));
        cuenta_beneficiario:=qryDatosNUM_CTABAN_PAGO_ELECT.Value;
        cuenta_beneficiario:=Format('%.*d',[20,strtoint64(cuenta_beneficiario)]);
        concepto_pago:='PAGO DE NOMINA DEL DIA '+datetostr(qryDatosFECHA.Value);
        email:=qryDatosEMAIL.Value;
        fecha:=FormatDateTime('YYYYMMDD',qryNominaFECHA_APLICACION.Value);
        rfc:=qryDatosRFC.Value;
        secuencia1:=secuencia1+1;
        WriteLn( F,'EE'+tipo_registro+'0400'+importe+fecha+'01'+inttostr(qryEmpleadoNUMERO.Value)+
        StringOfChar(' ',20-Length(inttostr(qryEmpleadoNUMERO.Value)))+'             '+nombre_beneficiario+
        Format('%.*d',[16,secuencia1])+StringOfChar('0',10)+cuenta_beneficiario+StringOfChar('0',5)+
        StringOfChar(' ',40)+'1'+' '+StringOfChar('0',5)+'044'+'044'+'001'+
        uppercase(concepto_pago)+StringOfChar(' ',50-Length(concepto_pago))+StringOfChar(' ',60)+StringOfChar('0',25)
        +StringOfChar(' ',22));

        //WriteLn(F,'EEDM'+email+StringOfChar(' ',100-Length(email))+StringOfChar('0',34)+StringOfChar(' ',232));
       end;
     end;
       qryDatos.Next;
  end;

  //PIE*************************************************
  conteo_DA_final:=Format('%.*d',[7,conteo_DA]);
  importe_DA_total:=importe_DA_total.replace('.','');
 // importe_DA_total:=Format('%.*d',[17,strtoint64(importe_DA_total)]);
    conteo_DB_final:=Format('%.*d',[7,conteo_DB]);
  importe_DB_total:=importe_DB_total.replace('.','');
  //importe_DB_total:=Format('%.*d',[17,strtoint64(importe_DB_total)]);
  WriteLn(F,'EETB'+conteo_DA_final+StringOfChar('0',17-Length(importe_DA_total))+importe_DA_total+conteo_DB_final+StringOfChar('0',17-Length(importe_DB_total))+importe_DB_total+StringOfChar('0',195)+StringOfChar(' ',123));
  WriteLn(F,'EETA'+conteo_DA_final+StringOfChar('0',17-Length(importe_DA_total))+importe_DA_total+conteo_DB_final+StringOfChar('0',17-Length(importe_DB_total))+importe_DB_total+StringOfChar('0',195)+StringOfChar(' ',123));
  CloseFile( F );

  qryDatos.First;
  ShowMessage('Archivo creado correctamente.');
 {except
  ShowMessage('ERROR no se pudo crear el archivo.');
 end; }

end;

procedure TPrincipal.crearArchivoTextoSantander;
var
importe,nombre_beneficiario,cuenta_beneficiario,concepto_pago,email,tipo_registro:string;
tipocuenta,num_banco_receptor,fecha,rfc:string;
importe_total,fecha_generacion:string;
consec:integer;
F:TextFile;
begin
//ABRIR CONSULTA
qryNomina.SQL.Clear;
qryNomina.SQL.Text:='select * from sic_dispersion where rfc=:rfc';
qryNomina.ParamByName('rfc').Value:=qryDatosSantanderRFC.Value;
qryNomina.Open;

//VARIABLEs
fecha_generacion:=formatdatetime('MMDDYYYY',Now);

//CREAR EL TXT
AssignFile( F, ExtractFilePath( Application.ExeName ) + 'Layout Santander.txt' );
  Rewrite( F );
  //ENCABEZADO*******************************************************
  WriteLn( F, '100001E'+fecha_generacion+{num de cuenta}'65505663319'+StringOfChar(' ',16-Length('65505663319'))+fecha_aplicacion);

 //**************************************DETALLADO//**********************************************************************
 importe_total:='0';
 try


 qryDatosSantander.First;
 consec:=1;
 while not qryDatosSantander.Eof do
      begin
       if qryDatosSantanderENTREGADO.Value='S' then
       begin
        if (Conexion.ExecSQLScalar('select count(num_empleado) from sic_dispersion where rfc=:id',[qryDatosSantanderRFC.Value]))=0 then
          begin
           ShowMessage('El empleado con el rfc: '+qryDatosSantanderRFC.Value+' no contiene numero de empleado.');
           qryNomina.Last;
           break;
          end
          else
        begin
       qryNomina.SQL.Text:='select * from sic_dispersion where rfc=:id';
       qryNomina.ParamByName('id').Value:=qryDatosSantanderRFC.Value;
       qryNomina.Open;
         //VARIABLES
        importe:=Floattostr((qryDatosSantanderTOTAL_PERCEP.Value)-(qryDatosSantanderTOTAL_RETEN.Value));
        importe_total:=floattostr(strtofloat(importe_total)+strtofloat(importe));
        importe:=Format('%.2f', [strtofloat(importe)]);
        importe:=importe.replace('.','');
        importe:=Format('%.*d',[18,strtoint64(importe)]);
        nombre_beneficiario:=(uppercase(qryDatosSantanderNOMBRES.Value))+' '+uppercase(qryDatosSantanderAPELLIDO_PATERNO.Value)+' '+uppercase(qryDatosSantanderAPELLIDO_MATERNO.Value);
        nombre_beneficiario:=nombre_beneficiario+StringOfChar(' ',40-Length(nombre_beneficiario));
        cuenta_beneficiario:=qryDatosSantanderNUM_CTABAN_PAGO_ELECT.Value;
        cuenta_beneficiario:=Format('%.*d',[20,strtoint64(cuenta_beneficiario)]);
        concepto_pago:='PAGO DE NOMINA DEL DIA '+datetostr(qryDatosSantanderFECHA.Value);
        email:=qryDatosSantanderEMAIL.Value;
        fecha:=FormatDateTime('YYYYMMDD',qryDatosSantanderFECHA.Value);
        rfc:=qryDatosSantanderRFC.Value;

        consec:=consec+1;

        WriteLn( F,'2'+Format('%.*d',[5,consec])+qryNominaNUM_EMPLEADO.Value+StringOfChar(' ',7-Length(qryNominaNUM_EMPLEADO.Value))+
        qryDatosSantanderAPELLIDO_PATERNO.Value+StringOfChar(' ',30-Length(qryDatosSantanderAPELLIDO_PATERNO.Value))+
        qryDatosSantanderAPELLIDO_MATERNO.Value+StringOfChar(' ',20-Length(qryDatosSantanderAPELLIDO_MATERNO.Value))+
        qryDatosSantanderNOMBRES.Value+StringOfChar(' ',30-Length(qryDatosSantanderNOMBRES.Value))+
        qryDatosSantanderNUM_CTABAN_PAGO_ELECT.Value+StringOfChar(' ',16-Length(qryDatosSantanderNUM_CTABAN_PAGO_ELECT.Value))+
        importe+'01');
       end;
     end;
       qryDatosSantander.Next;
  end;

 consec:=consec+1;
 importe_total:=Format('%.2f', [strtofloat(importe_total)]);
 importe_total:=importe_total.replace('.','');
// importe_total:=Format('%.*n',[18,strtofloat(importe_total)]);
 WriteLn(F,'3'+Format('%.*d',[5,consec])+Format('%.*d',[5,consec-2])+StringOfChar('0',18-Length(importe_total))+importe_total);

  CloseFile( F );

  qryDatosSantander.First;
  ShowMessage('Archivo creado correctamente.');
 except
  ShowMessage('ERROR: no se pudo crear el archivo.');
 end;

end;

function TPrincipal.obtenerSiglas (cadena : string) : string;
var
  siglas, palabra : string;
  i : integer;
begin
  siglas := '';
  palabra := '';
  for i := 1 to length(cadena) + 1 do
  begin
    if (cadena[i] <> ' ') and (i <= length(cadena)) then
        palabra := palabra + cadena[i]
    else
    begin
      if palabra <> '' then
      begin
        siglas := siglas + palabra[1];
        palabra := '';
      end;
    end;
  end;
  obtenerSiglas := siglas;
end;
end.
