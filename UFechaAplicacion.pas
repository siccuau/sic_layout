unit UFechaAplicacion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.StdCtrls;

type
  TFormFechaAplicacion = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    dtpFechaAplicacion: TDateTimePicker;
    btnAceptar: TButton;
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    fecha_aplicacion:string;
  end;

var
  FormFechaAplicacion: TFormFechaAplicacion;

implementation

{$R *.dfm}

procedure TFormFechaAplicacion.btnAceptarClick(Sender: TObject);
begin
fecha_aplicacion:=formatdatetime('MMDDYYYY',dtpFechaAplicacion.Date);
self.Close;
end;

end.
