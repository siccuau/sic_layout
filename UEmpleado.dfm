object FormEmpleado: TFormEmpleado
  Left = 0
  Top = 0
  Caption = 'Empleado'
  ClientHeight = 105
  ClientWidth = 291
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 21
    Top = 33
    Width = 139
    Height = 16
    Caption = 'Numero de Empleado:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 131
    Top = 8
    Width = 32
    Height = 16
    Caption = 'RFC: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object labelid: TLabel
    Left = 181
    Top = 8
    Width = 8
    Height = 16
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 92
    Width = 291
    Height = 13
    Panels = <
      item
        Width = 50
      end>
  end
  object btnGuardar: TButton
    Left = 86
    Top = 59
    Width = 81
    Height = 25
    Caption = 'Guardar'
    TabOrder = 1
    OnClick = btnGuardarClick
  end
  object txtNumeroEmpleado: TEdit
    Left = 167
    Top = 32
    Width = 121
    Height = 21
    MaxLength = 7
    TabOrder = 2
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=E:\Microsip datos 2018\AD2007.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 16
    Top = 182
  end
end
